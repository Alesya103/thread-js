import commentRepository from '../../data/repositories/commentRepository';
import NotFoundError from '../../helpers/errorHelper';

export const create = (userId, comment) => commentRepository.create({
  ...comment,
  userId
});

export const updateById = (commentId, comment) => commentRepository.updateById(commentId, comment);

export const deleteById = async commentId => {
  const isDeleted = await commentRepository.deleteById(commentId);
  if (!isDeleted) {
    throw new NotFoundError("Comment wasn't deleted", 404);
  }
  return 'Comment was deleted';
};

export const getCommentById = id => commentRepository.getCommentById(id);
