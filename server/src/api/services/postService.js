import postRepository from '../../data/repositories/postRepository';
import postReactionRepository from '../../data/repositories/postReactionRepository';
import NotFoundError from '../../helpers/errorHelper';
import sendEmail from '../utils/sendEmail';

export const getPosts = filter => postRepository.getPosts(filter);

export const getPostById = id => postRepository.getPostById(id);

export const create = (userId, post) => postRepository.create({
  ...post,
  userId
});

export const updateById = (postId, post) => postRepository.updateById(postId, post);

export const deleteById = async postId => {
  const isDeleted = await postRepository.deleteById(postId);
  if (!isDeleted) {
    throw new NotFoundError("Post wasn't deleted", 404);
  }
  return 'Post was deleted';
};

export const sendEmailById = async (postId, whoLiked) => {
  console.log(postId, whoLiked);
  await sendEmail('alesya.afanaseva27@gmail.com', 'bla', 'bla', '<h2>bla</h2>');
  return 'Email was sent';
};

export const setReaction = async (userId, { postId, isLike = true }) => {
  // define the callback for future use as a promise
  const updateOrDelete = react => (react.isLike === isLike
    ? postReactionRepository.deleteById(react.id)
    : postReactionRepository.updateById(react.id, { isLike }));

  const reaction = await postReactionRepository.getPostReaction(userId, postId);

  const result = reaction
    ? await updateOrDelete(reaction)
    : await postReactionRepository.create({
      userId,
      postId,
      isLike
    });

  // the result is an integer when an entity is deleted
  return Number.isInteger(result) ? {} : postReactionRepository.getPostReaction(userId, postId);
};
