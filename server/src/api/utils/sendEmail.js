import { sendgridEmail, sendgridKey } from '../../config/sendgridConfig';

const sgMail = require('@sendgrid/mail');

sgMail.setApiKey(sendgridKey);

export default async (to, subject, text, html) => {
  const msg = {
    to,
    from: sendgridEmail,
    subject,
    text,
    html
  };
  await sgMail.send(msg);
};
