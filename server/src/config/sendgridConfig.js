import env from '../env';

export const { sendgridKey, sendgridEmail } = env.sendgrid;
