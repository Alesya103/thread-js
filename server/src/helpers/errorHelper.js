export default class NotFoundError extends Error {
  constructor(message, status) {
    super(message);
    this.status = status;
    this.name = 'Not Found';
  }
}
