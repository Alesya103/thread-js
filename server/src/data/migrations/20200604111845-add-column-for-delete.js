export default {
  up: (queryInterface, Sequelize) => queryInterface.sequelize
    .query('CREATE EXTENSION IF NOT EXISTS pgcrypto;')
    .then(() => queryInterface.sequelize.transaction(transaction => Promise.all([
      queryInterface.addColumn('posts', 'deletedAt', {
        allowNull: true,
        type: Sequelize.DATE
      }, { transaction }),
      queryInterface.addColumn('comments', 'deletedAt', {
        allowNull: true,
        type: Sequelize.DATE
      }, { transaction })
    ]))),

  down: queryInterface => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.removeColumn('comments', { transaction }),
      queryInterface.removeColumn('posts', { transaction })
    ]))
};
